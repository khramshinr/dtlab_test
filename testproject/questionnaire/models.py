from uuid import uuid4

from django.db import models


class Questionnaire(models.Model):
    uuid = models.UUIDField(default=uuid4, db_index=True, unique=True, editable=False, help_text='UUID for this questionaire.')
    name = models.CharField(max_length=64, help_text='Questionnaire name.')
    description = models.TextField(null=True, blank=True, help_text='Questionnaire description.')

    def __str__(self):
        return self.name


class Question(models.Model):
    uuid = models.UUIDField(default=uuid4, db_index=True, unique=True, editable=False, help_text='UUID for this question.')
    questionnaire = models.ForeignKey('Questionnaire', on_delete=models.CASCADE, help_text='FK to Questionnaire')
    text = models.TextField(help_text='Question text.')
    is_first = models.BooleanField(default=False, help_text='Flag of the first question in the questionnaire')

    def __str__(self):
        return self.text


class Answer(models.Model):
    uuid = models.UUIDField(default=uuid4, db_index=True, unique=True, editable=False, help_text='UUID for this answer.')
    question_source = models.ForeignKey('Question', related_name='source', on_delete=models.CASCADE, help_text='FK to question for which the answer is intended')
    question_destination = models.ForeignKey('Question', null=True, blank=True, related_name='destination', on_delete=models.CASCADE, help_text='FK to question that is asked after the answer.')
    text = models.TextField(null=True, blank=True, help_text='Answer text.')
    additional_text = models.TextField(null=True, blank=True, help_text='Additional text. Will be sent to user when this answer is selected.')

    def __str__(self):
        return self.text




