from django.conf.urls import url

from .views import AnswerView
from .views import QuestionView
from .views import QuestionnaireView

app_name='questionnaire'
urlpatterns = [
    url(r'^$', QuestionnaireView.as_view({'get': 'list'}), name='questionnaire'),
    url(r'^question/(?P<uuid>.+)$', QuestionView.as_view({'get': 'retrieve'}), name='question'),
    url(r'^answer/(?P<uuid>.+)$', AnswerView.as_view({'get': 'retrieve'}), name='answer'),
]
