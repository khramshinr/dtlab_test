from django.apps import AppConfig


class QuestionnairConfig(AppConfig):
    name = 'questionnaire'
