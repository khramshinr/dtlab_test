from rest_framework.viewsets import ModelViewSet
from rest_framework.viewsets import ReadOnlyModelViewSet

from .models import Answer
from .models import Question
from .models import Questionnaire
from .serializers import AnswerSerializer
from .serializers import QuestionSerializer
from .serializers import QuestionnaireSerializer


class QuestionnaireView(ReadOnlyModelViewSet):
    queryset = Questionnaire.objects.all()
    serializer_class = QuestionnaireSerializer


class QuestionView(ReadOnlyModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    lookup_field = 'uuid'


class AnswerView(ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    lookup_field = 'uuid'


