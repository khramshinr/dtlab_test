from rest_framework import serializers

from .models import Question
from .models import Questionnaire
from .models import Answer


class QuestionnaireSerializer(serializers.ModelSerializer):
    question_uuid = serializers.SerializerMethodField(read_only=True, help_text='Questionnaire first question uuid.')

    class Meta:
        model = Questionnaire
        fields = ['uuid', 'question_uuid', 'name', 'description']

    def get_question_uuid(self, obj):
        try:
            return Question.objects.get(questionnaire=obj, is_first=True).uuid
        except Question.DoesNotExist:
            return None

    def to_representation(self, instance):
        self.context['request'].session.set_expiry(300)
        return super(QuestionnaireSerializer, self).to_representation(instance)


class QuestionSerializer(serializers.ModelSerializer):
    answers = serializers.SerializerMethodField(read_only=True, help_text='List of answer')

    class Meta:
        model = Question
        fields = ['uuid', 'text', 'answers']

    def get_answers(self, obj):
        answers = list()
        try:
            answers = list(Answer.objects.filter(question_source=obj).values('uuid', 'text'))
        except Answer.DoesNotExist:
            pass

        return answers

    def to_representation(self, instance):
        self.context['request'].session.set_expiry(300)
        if instance.is_first:
            self.context['request'].session['stack'] = instance.text + ':'
        return super(QuestionSerializer, self).to_representation(instance)


class AnswerSerializer(serializers.ModelSerializer):
    question_destination_uuid = serializers.SerializerMethodField(read_only=True, help_text='Destination question uuid')

    class Meta:
        model = Answer
        fields = ['uuid', 'text', 'additional_text', 'question_destination_uuid']

    def get_question_destination_uuid(self, obj):
        try:
            return Answer.objects.get(uuid=obj.uuid).question_destination.uuid
        except AttributeError:
            return None

    def to_representation(self, instance):
        self.context['request'].session.set_expiry(300)
        try:
            self.context['request'].session['stack'] = self.context['request'].session['stack'] +'->' + instance.text + ';'
        except KeyError:
            pass
        if instance.question_destination is None:
            try:
                print(self.context['request'].session['stack'])
            except KeyError:
                print('I can\'t show history because user don\'t use cookies.')
        return super(AnswerSerializer, self).to_representation(instance)

