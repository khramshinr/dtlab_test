from django.contrib import admin

from testproject.admin import admin_site

from .models import Questionnaire
from .models import Question
from .models import Answer


class QuestionInline(admin.TabularInline):
    model = Question
    extra = 0


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 0
    fk_name = 'question_source'


class QuestionnaireAdmin(admin.ModelAdmin):
    inlines = [QuestionInline]
    list_display = ('uuid', 'name', 'description')


class QuestionAdmin(admin.ModelAdmin):
    inlines = [AnswerInline]
    list_display = ('uuid', 'questionnaire', 'text', 'is_first')


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'text', 'additional_text', 'question_source', 'question_destination')


admin_site.register(Questionnaire, QuestionnaireAdmin)
admin_site.register(Question, QuestionAdmin)
admin_site.register(Answer, AnswerAdmin)