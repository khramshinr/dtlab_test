__all__ = ['QuestionnaireTestCase']
import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class QuestionnaireTestCase(APITestCase):
    fixtures = ['questionnaire/test.yaml']

    @classmethod
    def setUpClass(cls):
        super(QuestionnaireTestCase, cls).setUpClass()

        cls.question_with_answer = "bb9fd808-fbac-4a41-b0c6-bab77cbcebe9"
        cls.question_without_answer = "bb9fd808-fbac-4a41-b0c4-bab77cbcebe7"

        cls.aswer_with_dest_question = "bb9fd808-fbac-4a41-b0c6-bab79cbcebe7"
        cls.aswer_without_dest_question = "bb9fd807-fbac-4a41-b0c6-bab77cbcebe7"

    def _list_available_questionnaire(self):
        r = self.client.get(reverse('questionnaire:questionnaire'))
        return r

    def _get_question(self, uuid):
        r = self.client.get(path=reverse('questionnaire:question', kwargs=dict(uuid=uuid)))
        return r

    def _get_answer(self, uuid):
        r = self.client.get(path=reverse('questionnaire:answer', kwargs=dict(uuid=uuid)))
        return r

    def _test_answer_json_equal(self, obj, data):
        self.assertEqual(len(obj), len(data))

        if len(obj) > 0:
            self.assertSetEqual(set(obj[0].keys()), {'uuid', 'text'})

        self.assertListEqual(obj, data)

    def _test_questionnaire_json_equal(self, obj, data):
        self.assertSetEqual(set(obj.keys()), {'description', 'question_uuid', 'name', 'uuid'})
        self.assertEqual(obj['uuid'], data['uuid'])
        self.assertEqual(obj['question_uuid'], data['question_uuid'])
        self.assertEqual(obj['name'], data['name'])
        self.assertEqual(obj['description'], data['description'])

    def _test_question_json_equal(self, obj, data):
        self.assertSetEqual(set(obj.keys()), {'uuid', 'answers', 'text'})
        self.assertEqual(obj['uuid'], data['uuid'])
        self.assertEqual(obj['text'], data['text'])
        self._test_answer_json_equal(obj['answers'], json.loads(data['answers']))

    def _test_answer_resp_json_equal(self, obj, data):
        self.assertSetEqual(set(obj.keys()), {'uuid', 'text', 'additional_text', 'question_destination_uuid'})
        self.assertEqual(obj['uuid'], data['uuid'])
        self.assertEqual(obj['text'], data['text'])
        self.assertEqual(obj['additional_text'], data['additional_text'])
        self.assertEqual(obj['question_destination_uuid'], data['question_destination_uuid'])

    def test_list_available_questionnaire(self):
        r = self._list_available_questionnaire()
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(3, len(r.json()))
        return r

    def test_data_first_available_questionnaire(self):
        data = {'uuid': 'bb9fd808-fbac-4a41-b0c6-bab77cbcebe7',
                'question_uuid': 'bb9fd808-fbac-4a41-b0c6-bab77cbcebe9',
                'name': 'First',
                'description': 'First questionnaire'}

        r = self.test_list_available_questionnaire()

        obj = r.json()[0]
        self._test_questionnaire_json_equal(obj, data)

    def test_get_question_with_answer(self):
        data = {
            'uuid': f'{self.question_with_answer}',
            'text': 'Are you hungry?',
            'answers': '[{"uuid": "bb9fd808-fbac-4a41-b0c6-bab79cbcebe7", "text": "Yes"}, '
                       '{"uuid": "bb9fd807-fbac-4a41-b0c6-bab77cbcebe7", "text": "No"}]'
        }
        r = self._get_question(self.question_with_answer)
        self.assertEqual(r.status_code, status.HTTP_200_OK)

        obj = r.json()
        self._test_question_json_equal(obj, data)

    def test_get_question_without_answer(self):
        data = {
            'uuid': f'{self.question_without_answer}',
            'text': 'Are you hungry 2?',
            'answers': '[]'
        }
        r = self._get_question(self.question_without_answer)
        self.assertEqual(r.status_code, status.HTTP_200_OK)

        obj = r.json()
        self._test_question_json_equal(obj, data)

    def test_get_question_with_baduuid(self):
        data = {}
        r = self._get_question('BAD_UUID')
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_question_with_not_exist_uuid(self):
        data = {}
        r = self._get_question('bb9fd808-fbac-4a41-b0c6-bab79cbdfbe7')
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)

    def test_put_answer_with_dest_question(self):
        data = {
            'uuid': f'{self.aswer_with_dest_question}',
            'text': 'Yes',
            'additional_text': '',
            'question_destination_uuid': 'bb9fd808-fbac-4a41-b0c6-bab77cbcebe0'
        }
        r = self._get_answer(self.aswer_with_dest_question)
        self.assertEqual(r.status_code, status.HTTP_200_OK)

        obj = r.json()
        self._test_answer_resp_json_equal(obj, data)

    def test_put_answer_without_dest_question(self):
        data = {
            'uuid': f'{self.aswer_without_dest_question}',
            'text': 'No',
            'additional_text': 'Call me when you\'re hungry.',
            'question_destination_uuid': None
        }
        r = self._get_answer(self.aswer_without_dest_question)
        self.assertEqual(r.status_code, status.HTTP_200_OK)

        obj = r.json()
        self._test_answer_resp_json_equal(obj, data)

    def test_put_answer_with_bad_uuid(self):
        data = {}
        r = self._get_answer('BAD_UUID')
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)

    def test_put_answer_with_not_exist_uuid(self):
        data = {}
        r = self._get_answer('acdcd808-fbac-4a41-b0c6-bab79cbdfbe7')
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)

