import json

from django import forms
from django.contrib import messages as django_messages
from django.contrib.admin import AdminSite
from django.db import transaction
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import path
from django.urls import reverse

from questionnaire.models import Answer
from questionnaire.models import Question
from questionnaire.models import Questionnaire


class LoadQuestionnaireForm(forms.Form):
    file = forms.FileField(label='Select a file')


class TestProjectAdminSite(AdminSite):
    index_template = 'testproject/admin_index.html'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.botmd_user = None

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('load_questionnaire/', self.load_questionnaire, name='load_questionnaire'),
        ]
        return my_urls + urls

    def load_questionnaire(self, request):
        return self._process_form(
            request,
            LoadQuestionnaireForm(request.POST, request.FILES) if request.method == 'POST' else LoadQuestionnaireForm(),
            'load_questionnaire'
        )

    def _process_form(self, request, form, key):
        if request.method == 'POST':
            if form.is_valid():
                data = form.files['file'].read()

                for idx, questionnaires in enumerate(json.loads(data)):
                    try:
                        self.create_questionnaire(questionnaires)
                        django_messages.add_message(request, django_messages.SUCCESS, f'Load questionnaire: "{questionnaires["name"]}" success!')
                    except Exception as e:
                        django_messages.add_message(request, django_messages.ERROR, f'Load questionnaire with index {idx} failed! Check your file.')

            return HttpResponseRedirect(reverse(f'admin:{key}'))

        else:
            context = dict(
                self.each_context(request),
                form=form,
                opts=Questionnaire._meta
            )

            return TemplateResponse(request, f'testproject/{key}.html', context)

    @transaction.atomic
    def create_questionnaire(self, questionnaires):
        # fill questionnaires
        questionnaire = Questionnaire(name=questionnaires['name'],
                                      description=questionnaires['description'])
        questionnaire.save()
        question_map = dict()

        # fill questions
        for questions in questionnaires['questions']:
            question = Question(questionnaire=questionnaire,
                                text=questions['text'],
                                is_first=questions['is_first'])
            question.save()
            question_map[questions['number']] = question

        # fill answers
        for questions in questionnaires['questions']:
            for answers in questions['answers']:
                answer = Answer(question_source=question_map[questions['number']],
                                question_destination=question_map[answers['question_destination']] if answers[
                                                                                                          'question_destination'] is not None else None,
                                text=answers['text'],
                                additional_text=answers['additional_text'])
                answer.save()


admin_site = TestProjectAdminSite(name='testprojectadmin')
